﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookPostSales.values
{
    public enum ReturnStatus
    {
        SUCCESS,
        FAIL,
        COOKIE_DIE,
        CHECKPOINT,
        BLOCK
    }

    public class ReturnResult
    {
        public string Uid { get; set; }
        public List<string> CookieSuccess { get; set; }
        public List<string> CookiePending { get; set; }
        public List<string> CookieSpam { get; set; }
        public ReturnStatus Status { get; set; }
    }
}
