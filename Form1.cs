﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Chilkat;
using ChilkatSshManager;
using FacebookPostSales.dtos;
using FacebookPostSales.helpers;
using FacebookPostSales.models;
using FacebookPostSales.values;
using Microsoft.Win32;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace FacebookPostSales
{
    public partial class frmMain : Form
    {
        List<BaiDangDto> BaiDangDtoMain = new List<BaiDangDto>();
        List<BaiDangDto> BaiDangDtos = new List<BaiDangDto>();
        List<BaiDangDto> BaiDangDtoss = new List<BaiDangDto>();
        List<string> cloneList = new List<string>();
        List<Thread> Threads = new List<Thread>();
        List<IWebDriver> FirefoxDrivers = new List<IWebDriver>();
        List<SshTunnel> SshTunnels = new List<SshTunnel>();
        ConfigDto configDto = new ConfigDto();
        BaiDangDto baiDangDtoMain = new BaiDangDto();
        Random random = new Random();
        public static string datenow = "", pathPortable = "";

        private int timePostFrom = 0,
            timePostTo = 0,
            numberPostPause = 0,
            timePostPause = 0,
            numberGroup = 0,
            numberThread = 0,
            success = 0,
            fail = 0;
        public static object lock_up_image = new object();

        private bool groupRandomChecked = false, repeatChecked = false, postRandomChecked = false;
        private RegistryKey keyOpen = null, keyCreate = null;

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            if (!ChilKatSsh.Load("4ynUie.SSX0220_hlBDUJPA9T3B"))
            {
                MessageBox.Show("Lỗi thư viện kết nối ssh");
                return;
            }

            try
            {
                lstLoger.View = View.Details;

                lstLoger.Columns.Add("#", 30);
                lstLoger.Columns.Add("Uid", 120);
                lstLoger.Columns.Add("Trạng thái", 120);
                lstLoger.FullRowSelect = true;
            }
            catch { }

            try
            {
                lstListPost.View = View.Details;
                lstListPost.Columns.Add(null, 30);
                lstListPost.Columns.Add("Tên", 150);
                lstListPost.FullRowSelect = true;
            }
            catch { }

            Size = new Size(701, 587);

            keyCreate = Registry.CurrentUser.CreateSubKey(@"Control Panel\Desktop\FacebookPostSales\OurSettings");
            keyOpen = Registry.CurrentUser.OpenSubKey(@"Control Panel\Desktop\FacebookPostSales\OurSettings");

            OpenAllKey();

            try
            {
                var dirs = Directory.GetDirectories(pathPortable);
                cloneList.AddRange(dirs);
                lblTongClone.Text = cloneList.Count.ToString();

            }
            catch
            {
                //
            }

            sshList = FunctionHelper.LoadFile("Du lieu dau vao/Ssh_List.txt");

            BaiDangDtos = FunctionHelper.LoadDanhSach(lstListPost);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            timePostFrom = Convert.ToInt32(numTimePostFrom.Text);
            timePostTo = Convert.ToInt32(numTimePostTo.Text);
            numberPostPause = Convert.ToInt32(numPausePost.Text);
            timePostPause = Convert.ToInt32(numPauseTime.Text);
            numberGroup = Convert.ToInt32(numGroupSwitch.Text);
            numberThread = Convert.ToInt32(numThread.Text);
            groupRandomChecked = ckbRandomGroup.Checked;
            repeatChecked = ckbLapLai.Checked;
            postRandomChecked = ckbRandomPost.Checked;

            if (!postRandomChecked)
            {
                BaiDangDtoMain = BaiDangDtoss;
            }
            else
            {
                BaiDangDtoMain = BaiDangDtos;
            }

            configDto.TimePostFrom = timePostFrom;
            configDto.TimePostTo = timePostTo;
            configDto.NumberPostPause = numberPostPause;
            configDto.TimePostPause = timePostPause;
            configDto.NumberGroup = numberGroup;
            configDto.NumberThread = numberThread;
            configDto.GroupRandomChecked = groupRandomChecked;
            configDto.RepeatChecked = repeatChecked;
            configDto.PostRandomChecked = postRandomChecked;

            pathPortable = txtPathPortable.Text.Trim();

            SaveAllKey();

            foreach (ListViewItem item in lstListPost.Items)
            {
                if (item.Checked)
                {
                    foreach (BaiDangDto dto in BaiDangDtos)
                    {
                        if (dto.TenLuu == item.SubItems[1].Text)
                        {
                            BaiDangDtoss.Add(dto);
                            break;
                        }
                    }
                }
            }

            //OpenBrowserModel.ExtractToDirectory(numberThread);

            //pathFirefoxList.Clear();
            //var list = Directory.GetDirectories("Profile");
            //var list = Directory.GetDirectories(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).Replace("\\Roaming", "") + "\\Local\\Google\\Chrome\\User Data");
            //foreach (string s in list)
            //{
            //    string fullName = new DirectoryInfo(s).Name;
            //    if (fullName.Split(' ')[0] == "Profile")
            //    {
            //        pathFirefoxList.Add(new DirectoryInfo(s).FullName);
            //    }
            //}
            //lblTongClone.Text = pathFirefoxList.Count.ToString();

            Thread thread = new Thread(() => MultiThread());
            thread.IsBackground = true;
            thread.Start();

            Threads.Insert(0, thread);

            btnStart.Visible = false;
            btnStop.Visible = true;
        }

        private void MultiThread()
        {
            for (int i = 0; i < numberThread; i++)
            {
                Thread thread = new Thread(() => OneThread());
                thread.IsBackground = true;
                thread.Start();

                Threads.Insert(0, thread);

                Thread.Sleep(3000);
            }
        }

        object lock_cookie = new object();

        private void btnStop_Click(object sender, EventArgs e)
        {
            btnStop.Visible = false;

            foreach (Thread thread in Threads)
            {
                thread.Abort();
            }

            try
            {
                foreach (FirefoxDriver driver in FirefoxDrivers)
                {
                    try
                    {
                        driver.Quit();
                    }
                    catch { }
                }
            }
            catch { }

            btnStart.Visible = true;
        }

        object lock_post = new object();

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (Thread thread in Threads)
            {
                thread.Abort();
            }

            try
            {
                foreach (FirefoxDriver driver in FirefoxDrivers)
                {
                    try
                    {
                        driver.Quit();
                    }
                    catch { }
                }
            }
            catch { }
        }

        private int x = 0, y = 0, X = SystemInformation.VirtualScreen.Width;

        private void lstListPost_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void lstListPost_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void lstListPost_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column == 0)
            {
                bool value = false;
                try
                {
                    value = Convert.ToBoolean(this.lstListPost.Columns[e.Column].Tag);
                }
                catch (Exception)
                {
                }
                this.lstListPost.Columns[e.Column].Tag = !value;
                foreach (ListViewItem item in this.lstListPost.Items)
                    item.Checked = !value;

                this.lstListPost.Invalidate();
            }
        }

        private void lstListPost_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                string name = lstListPost.SelectedItems[0].SubItems[1].Text;
                foreach (BaiDangDto dto in BaiDangDtos)
                {
                    if (dto.TenLuu == name)
                    {
                        txtMatHang.Text = dto.MatHang;
                        txtGia.Text = dto.Gia;
                        txtViTri.Text = dto.ViTri;
                        txtNoiDung.Text = dto.NoiDung;
                        txtAnh.Text = dto.Anh;
                        txtTenLuuBaiDang.Text = dto.TenLuu;

                        break;
                    }
                }
            }
            catch { }
        }

        private void lstListPost_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.DrawBackground();
                bool value = false;
                try
                {
                    value = Convert.ToBoolean(e.Header.Tag);
                }
                catch (Exception)
                {
                }
                CheckBoxRenderer.DrawCheckBox(e.Graphics,
                    new Point(e.Bounds.Left + 4, e.Bounds.Top + 4),
                    value ? System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal :
                        System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal);
            }
            else
            {
                e.DrawDefault = true;
            }
        }

        private void btnLoger_Click(object sender, EventArgs e)
        {
            if (btnLoger.Text.Contains(">>"))
            {
                Size = new Size(1012, 587);
                btnLoger.Text = "<< Ẩn Loger";
            }
            else
            {
                Size = new Size(701, 587);
                btnLoger.Text = "Hiện Loger >>";
            }
        }

        private int port = 1090;

        private void txtPathPortable_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtPathPortable_DoubleClick(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                pathPortable = dialog.SelectedPath;
                txtPathPortable.Text = pathPortable;
            }
        }

        List<string> sshList = new List<string>();
        object lock_ssh = new object();
        object lock_path = new object();
        List<string> pathFirefoxList = new List<string>();
        private void OneThread()
        {
            int xx = 0, yy = 0, p = 0;
            datenow = FunctionHelper.GetDateNow();
            bool openBrowser = true, position = true;
            IWebDriver driver = null;
            string cookie = "", uid = "";
            string[] account = null, ssh = null;

            string path = "";

            while (true)
            {
                try
                {
                    lock (lock_path)
                    {
                        if (cloneList.Count == 0 && repeatChecked)
                        {
                            cloneList = FunctionHelper.LoadFile("Du lieu dau ra/Uid_Finish.txt");
                        }
                        path = cloneList[0];
                        uid = path.Split('|')[0];
                        cloneList.RemoveAt(0);
                        Invoke(new MethodInvoker(() => { lblTongClone.Text = cloneList.Count.ToString(); }));
                    }
                }
                catch
                {
                    break;
                }
                //if (sshList.Count == 0)
                //{
                //    break;
                //}

                //try
                //{
                //    lock (lock_cookie)
                //    {
                //        if (cloneList.Count == 0)
                //        {
                //            if (repeatChecked)
                //            {
                //                cloneList = FunctionHelper.LoadFile("Du lieu dau ra/Cookie_Finish.txt");
                //            }
                //            File.WriteAllText("Du lieu dau ra/Cookie_Finish.txt", "");
                //        }

                //        cookie = cloneList[0];

                //        cloneList.RemoveAt(0);
                //        Invoke(new MethodInvoker(() => { lblTongClone.Text = cloneList.Count.ToString(); }));
                //    }
                //}
                //catch
                //{
                //    break;
                //}

                //account = cookie.Split('|');

                //SshTunnel tunnel = new SshTunnel();
                //while (sshList.Count != 0)
                //{
                //    if (tunnel.IsSshConnected())
                //    {
                //        break;
                //    }
                //    lock (lock_ssh)
                //    {
                //        ssh = sshList[0].Split('|');
                //        sshList.RemoveAt(0);
                //    }
                //    if (ChilKatSsh.Connect(tunnel, ssh[0], ssh[1], ssh[2], p))
                //    {
                //        break;
                //    }
                //}

                //if (!tunnel.IsSshConnected())
                //{
                //    continue;
                //}

                //SshTunnels.Add(tunnel);
                //Console.WriteLine("connected " + ssh[0] + " Port " + p);

                if (!Directory.Exists(pathPortable + "\\" + uid))
                {
                    continue;
                }

                if (openBrowser)
                {
                    driver = OpenBrowserModel.ChromePortable(uid, p, true);
                    //driver = OpenBrowserModel.OpenFirefoxPortable(p, path);
                    //driver.Manage().Window.Position = new Point(xx, yy);
                    FirefoxDrivers.Add(driver);
                }

                try
                {
                    //string ck = "datr=QxLbXCcEFHkZ7BU4_wNHbAeM; sb=lZrbXIHB-qouyWV0Aj-PaGQs; wd=1920x937; c_user=100003814810201; xs=8%3A50QTT8MMbIRPzA%3A2%3A1558258276%3A12401%3A6389; spin=r.1000730029_b.trunk_t.1558258300_s.1_v.2_; fr=1v8C686i6iIP69gno.AWVaUdGVa6onZV8iHZHaVELLEFw.BcmLw-.67.FzW.0.0.Bc4YkX.AWUii8-6; presence=EDvF3EtimeF1558284565EuserFA21B03814810201A2EstateFDutF1558284565927CEchFDp_5f1B03814810201F2CC; ";
                    OneThreadModel threadModel = new OneThreadModel(this, lstLoger, driver, uid, "", configDto);
                    ReturnResult result = threadModel.Start(BaiDangDtoMain);
                    if (result.Status == ReturnStatus.SUCCESS)
                    {
                        Interlocked.Increment(ref success);
                        Invoke(new MethodInvoker(() => { lblOkClone.Text = success.ToString(); }));

                        lock (lock_cookie)
                        {
                            File.WriteAllLines("Du lieu dau vao/Uid_List.txt", cloneList);
                            File.AppendAllText("Du lieu dau ra/Uid_Finish.txt", uid + Environment.NewLine);

                            if (result.CookieSuccess.Count != 0)
                            {
                                File.AppendAllLines($"Du lieu dau ra/{result.Uid}_success_{datenow}.txt", result.CookieSuccess);
                            }
                            if (result.CookiePending.Count != 0)
                            {
                                File.AppendAllLines($"Du lieu dau ra/{result.Uid}_pending_{datenow}.txt", result.CookiePending);
                            }
                            if (result.CookieSpam.Count != 0)
                            {
                                File.AppendAllLines($"Du lieu dau ra/{result.Uid}_spam_{datenow}.txt", result.CookieSpam);
                            }
                        }
                    }
                    else if (result.Status == ReturnStatus.CHECKPOINT)
                    {
                        Interlocked.Increment(ref fail);
                        Invoke(new MethodInvoker(() => { lblFailClone.Text = fail.ToString(); }));

                        lock (lock_cookie)
                        {
                            File.WriteAllLines("Du lieu dau vao/Uid_List.txt", cloneList);
                            File.AppendAllText("Du lieu dau ra/uid_checkpoint.txt", result.Uid + "_" + datenow + Environment.NewLine);
                        }
                    }
                    else if (result.Status == ReturnStatus.COOKIE_DIE)
                    {
                        Interlocked.Increment(ref fail);
                        Invoke(new MethodInvoker(() => { lblFailClone.Text = fail.ToString(); }));

                        lock (lock_cookie)
                        {
                            File.WriteAllLines("Du lieu dau vao/Uid_List.txt", cloneList);
                            File.AppendAllText("Du lieu dau ra/uid_die.txt", result.Uid + "_" + datenow + Environment.NewLine);
                        }
                    }
                    else if (result.Status == ReturnStatus.BLOCK)
                    {
                        Interlocked.Increment(ref fail);
                        Invoke(new MethodInvoker(() => { lblFailClone.Text = fail.ToString(); }));

                        lock (lock_cookie)
                        {
                            File.WriteAllLines("Du lieu dau vao/Uid_List.txt", cloneList);
                            File.AppendAllText("Du lieu dau ra/uid_block.txt", result.Uid + "_" + datenow + Environment.NewLine);
                        }
                    }
                    else if (result.Status == ReturnStatus.FAIL)
                    {
                        Interlocked.Increment(ref fail);
                        Invoke(new MethodInvoker(() => { lblFailClone.Text = fail.ToString(); }));

                        lock (lock_cookie)
                        {
                            cloneList.Insert(0, cookie);
                            File.WriteAllLines("Du lieu dau vao/Uid_List.txt", cloneList);
                        }
                    }

                    try
                    {
                        FirefoxDrivers.Remove(driver);
                        driver.Quit();
                    }
                    catch { }

                    //try
                    //{
                    //    SshTunnels.Remove(tunnel);
                    //    ChilKatSsh.Disconnect(tunnel);
                    //}
                    //catch
                    //{
                    //    //
                    //}
                }
                catch
                {
                    Console.WriteLine("Co loi gi do admin oiiiiiiiiiiiiiiiiiii");

                    try
                    {
                        FirefoxDrivers.Remove(driver);
                        driver.Quit();
                    }
                    catch { }

                    //try
                    //{
                    //    SshTunnels.Remove(tunnel);
                    //    ChilKatSsh.Disconnect(tunnel);
                    //}
                    //catch
                    //{
                    //    //
                    //}
                }
            }

            Interlocked.Decrement(ref numberThread);

            if (numberThread == 0)
            {
                MessageBox.Show("Tool đã chạy xong", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Invoke(new MethodInvoker(() =>
                {
                    btnStart.Visible = true;
                    btnStop.Visible = false;
                }));
            }
        }

        private void OpenAllKey()
        {
            if (keyOpen != null)
            {
                if (keyOpen.GetValue("timePostFrom") != null)
                {
                    numTimePostFrom.Text = keyOpen.GetValue("timePostFrom").ToString();
                }
                if (keyOpen.GetValue("timePostTo") != null)
                {
                    numTimePostTo.Text = keyOpen.GetValue("timePostTo").ToString();
                }
                if (keyOpen.GetValue("numberPostPause") != null)
                {
                    numPausePost.Text = keyOpen.GetValue("numberPostPause").ToString();
                }
                if (keyOpen.GetValue("timePostPause") != null)
                {
                    numPauseTime.Text = keyOpen.GetValue("timePostPause").ToString();
                }
                if (keyOpen.GetValue("numberGroup") != null)
                {
                    numGroupSwitch.Text = keyOpen.GetValue("numberGroup").ToString();
                }
                if (keyOpen.GetValue("numberThread") != null)
                {
                    numThread.Text = keyOpen.GetValue("numberThread").ToString();
                }
                if (keyOpen.GetValue("groupRandomChecked") != null)
                {
                    ckbRandomGroup.Checked = Boolean.Parse(keyOpen.GetValue("groupRandomChecked").ToString());
                }
                if (keyOpen.GetValue("repeatChecked") != null)
                {
                    ckbLapLai.Checked = Boolean.Parse(keyOpen.GetValue("repeatChecked").ToString());
                }
                if (keyOpen.GetValue("postRandomChecked") != null)
                {
                    ckbRandomPost.Checked = Boolean.Parse(keyOpen.GetValue("postRandomChecked").ToString());
                }
                if (keyOpen.GetValue("pathPortable") != null)
                {
                    txtPathPortable.Text = keyOpen.GetValue("pathPortable").ToString();
                }
            }
        }

        private void SaveAllKey()
        {
            keyCreate.SetValue("timePostFrom", timePostFrom);
            keyCreate.SetValue("timePostTo", timePostTo);
            keyCreate.SetValue("numberPostPause", numberPostPause);
            keyCreate.SetValue("timePostPause", timePostPause);
            keyCreate.SetValue("numberGroup", numberGroup);
            keyCreate.SetValue("numberThread", numberThread);
            keyCreate.SetValue("groupRandomChecked", groupRandomChecked);
            keyCreate.SetValue("repeatChecked", repeatChecked);
            keyCreate.SetValue("postRandomChecked", postRandomChecked);
            keyCreate.SetValue("pathPortable", pathPortable);
        }

        private void btnThemBaiDang_Click(object sender, EventArgs e)
        {
            BaiDangDto baiDangDto = new BaiDangDto();
            baiDangDto.MatHang = txtMatHang.Text;
            baiDangDto.Gia = txtGia.Text;
            baiDangDto.NoiDung = txtNoiDung.Text;
            baiDangDto.ViTri = txtViTri.Text;
            baiDangDto.Anh = txtAnh.Text;
            baiDangDto.TenLuu = txtTenLuuBaiDang.Text;

            if (!FunctionHelper.KiemTraTenLuu(BaiDangDtos, baiDangDto.TenLuu))
            {
                MessageBox.Show("Tên lưu này đã tồn tại, vui lòng chọn tên khác", "Thông báo", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            BaiDangDtos.Add(baiDangDto);

            try
            {
                FunctionHelper.LuuBaiDangMoi(baiDangDto);

                FunctionHelper.CapNhatDanhSach(BaiDangDtos, lstListPost);

                MessageBox.Show("Lưu bài đăng mới thành công", "Chúc mừng", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

                DeleteAllTextbox();
            }
            catch
            {
                MessageBox.Show("Kiểm tra lại tên lưu, có chứa ký tự đặc biệt không nhé!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtAnh_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = true;
            dialog.Title = "Chọn ảnh";
            dialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                txtAnh.Text = "";
                foreach (string fileName in dialog.FileNames)
                {
                    txtAnh.Text += $"{fileName},";
                }
            }
        }

        private void btnSuaBaiDang_Click(object sender, EventArgs e)
        {
            foreach (BaiDangDto dangDto in BaiDangDtos)
            {
                if (dangDto.TenLuu == txtTenLuuBaiDang.Text)
                {
                    BaiDangDtos.Remove(dangDto);

                    dangDto.MatHang = txtMatHang.Text;
                    dangDto.Gia = txtGia.Text;
                    dangDto.NoiDung = txtNoiDung.Text;
                    dangDto.ViTri = txtViTri.Text;
                    dangDto.Anh = txtAnh.Text;

                    BaiDangDtos.Add(dangDto);

                    FunctionHelper.LuuBaiDangMoi(dangDto);
                    break;
                }
            }

            try
            {
                FunctionHelper.CapNhatDanhSach(BaiDangDtos, lstListPost);

                MessageBox.Show("Sửa bài đăng thành công", "Chúc mừng", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Kiểm tra lại tên lưu, có chứa ký tự đặc biệt không nhé!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXoaBaiDang_Click(object sender, EventArgs e)
        {
            try
            {
                string name = lstListPost.SelectedItems[0].SubItems[1].Text;
                if (!string.IsNullOrEmpty(name))
                {
                    foreach (BaiDangDto dto in BaiDangDtos)
                    {
                        if (dto.TenLuu == name)
                        {
                            BaiDangDtos.Remove(dto);
                            File.Delete($"Danh sach bai dang/{dto.TenLuu}.txt");
                            FunctionHelper.CapNhatDanhSach(BaiDangDtos, lstListPost);
                            break;
                        }
                    }
                }
            }
            catch
            {
            }

            DeleteAllTextbox();
        }

        private void DeleteAllTextbox()
        {
            txtMatHang.Text = "";
            txtGia.Text = "";
            txtViTri.Text = "";
            txtNoiDung.Text = "";
            txtAnh.Text = "Nháp đúp để chọn ảnh";
            txtTenLuuBaiDang.Text = "";
        }

        private void btnThemClone_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = true;
            dialog.Title = "Chọn danh sách uid";
            dialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                cloneList.Clear();
                foreach (string fileName in dialog.FileNames)
                {
                    foreach (string line in File.ReadAllLines(fileName))
                    {
                        if (!string.IsNullOrEmpty(line))
                        {
                            cloneList.Add(line);
                        }
                    }
                }

                lblTongClone.Text = cloneList.Count.ToString();
                File.WriteAllLines("Du lieu dau vao/Uid_List.txt", cloneList);
            }
        }

    }
}
