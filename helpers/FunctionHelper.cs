﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacebookPostSales.dtos;
using OpenQA.Selenium.Firefox;

namespace FacebookPostSales.helpers
{
    public static class FunctionHelper
    {
        public static List<BaiDangDto> LoadDanhSach(ListView listView)
        {
            List<BaiDangDto> baiDangDto = new List<BaiDangDto>();

            var dir = Directory.GetFiles("Danh sach bai dang");
            for (int i = 0; i < dir.Length; i++)
            {
                var info = new DirectoryInfo(dir[i]);
                string[] line = File.ReadAllLines(info.FullName);
                BaiDangDto dto = new BaiDangDto();
                dto.MatHang = line[0];
                dto.Gia = line[1];
                dto.ViTri = line[2];
                for (int j = 3; j < line.Length - 1; j++)
                {
                    if (j == line.Length - 2)
                    {
                        dto.NoiDung += line[j];
                    }
                    else
                    {
                        dto.NoiDung += line[j] + Environment.NewLine;
                    }
                }
                dto.Anh = line[line.Length - 1];
                dto.TenLuu = info.Name.Replace(".txt", "");

                baiDangDto.Add(dto);

                string[] arr2 = new string[2];
                for (int j = 0; j < arr2.Length; j++)
                {
                    arr2[j] = string.Empty;
                }
                arr2[0] = null;
                arr2[1] = dto.TenLuu;
                listView.Items.Add(new ListViewItem(arr2));
            }

            return baiDangDto;
        }

        public static void LuuBaiDangMoi(BaiDangDto baiDangDto)
        {
            string post = "";
            post += baiDangDto.MatHang + Environment.NewLine;
            post += baiDangDto.Gia + Environment.NewLine;
            post += baiDangDto.ViTri + Environment.NewLine;
            post += baiDangDto.NoiDung + Environment.NewLine;
            post += baiDangDto.Anh;

            File.WriteAllText($"Danh sach bai dang/{baiDangDto.TenLuu}.txt", post);
        }

        public static void CapNhatDanhSach(List<BaiDangDto> baiDangDto, ListView listView)
        {
            listView.Invoke(new MethodInvoker((() => { listView.Items.Clear(); })));
            foreach (BaiDangDto dangDto in baiDangDto)
            {
                string[] arr2 = new string[2];
                for (int j = 0; j < arr2.Length; j++)
                {
                    arr2[j] = string.Empty;
                }
                arr2[0] = null;
                arr2[1] = dangDto.TenLuu;
                listView.Items.Add(new ListViewItem(arr2));
            }
        }

        public static bool KiemTraTenLuu(List<BaiDangDto> baiDangDto, string name)
        {
            foreach (BaiDangDto dto in baiDangDto)
            {
                if (dto.TenLuu == name)
                {
                    return false;
                }
            }

            return true;
        }

        public static string RandomString(int num)
        {
            char[] sum = "qwertyuiopasdfghjklzxcvbnm".ToCharArray();
            string output = "";
            for (int i = 0; i < num; i++)
            {
                output += sum[new Random().Next(sum.Length)];
                Thread.Sleep(100);
            }

            return output;
        }

        public static string RandomChara(int num)
        {
            char[] sum = ",.!';:".ToCharArray();
            string output = "";
            for (int i = 0; i < num; i++)
            {
                output += sum[new Random().Next(sum.Length)];
                Thread.Sleep(100);
            }

            return output;
        }

        public static string RandomIcon(int num)
        {
            char[] sum = "✪✩✰✫✬✭★⋆✢✣✤✥❋✦✧✱✶✷✴✸✺✻❇❈❉❊✽✾✿❁❃❋❀".ToCharArray();
            string output = "";
            for (int i = 0; i < num; i++)
            {
                output += sum[new Random().Next(sum.Length)];
                Thread.Sleep(100);
            }

            return output;
        }

        public static List<string> LoadFile(string path)
        {
            List<string> arrList = new List<string>();
            if (File.Exists(path))
            {
                foreach (string line in File.ReadAllLines(path))
                {
                    if (line.Trim() != "")
                    {
                        arrList.Add(line);
                    }
                }
            }

            return arrList;
        }

        public static string GetDateNow()
        {
            DateTime time = DateTime.Now;

            return "" + time.Day + time.Month + time.Year;
        }

        public static void AddItemToListView(ListView lstLoger, string uid, string status)
        {
            string[] arr1 = new string[3];
            for (int i = 0; i < arr1.Length; i++)
            {
                arr1[i] = string.Empty;
            }

            arr1[0] = (lstLoger.Items.Count + 1).ToString();
            arr1[1] = uid;
            arr1[2] = status;

            lstLoger.BeginInvoke(new MethodInvoker(() =>
            {
                lstLoger.BeginUpdate();
                lstLoger.Items.Add(new ListViewItem(arr1));
                lstLoger.EndUpdate();
                lstLoger.Items[lstLoger.Items.Count - 1].EnsureVisible();
            }));
        }
    }
}
