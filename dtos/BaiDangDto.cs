﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookPostSales.dtos
{
   public class BaiDangDto
    {
        public string MatHang { get; set; }
        public string Gia { get; set; }
        public string ViTri { get; set; }
        public string NoiDung { get; set; }
        public string Anh { get; set; }
        public string TenLuu { get; set; }
    }
}
