﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookPostSales.dtos
{
    public class ConfigDto
    {
        public string Cookie { get; set; }

        public int TimePostFrom { get; set; }
        public int TimePostTo { get; set; }
        public int NumberPostPause { get; set; }
        public int TimePostPause { get; set; }
        public int NumberGroup { get; set; }
        public int NumberThread { get; set; }
        public bool GroupRandomChecked { get; set; }
        public bool RepeatChecked { get; set; }
        public bool PostRandomChecked { get; set; }
    }
}
