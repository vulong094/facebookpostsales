﻿namespace FacebookPostSales
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnLoger = new iTalk.iTalk_Button_1();
            this.iTalk_GroupBox4 = new iTalk.iTalk_GroupBox();
            this.lstLoger = new iTalk.iTalk_Listview();
            this.btnStart = new iTalk.iTalk_Button_2();
            this.btnStop = new iTalk.iTalk_Button_1();
            this.iTalk_GroupBox3 = new iTalk.iTalk_GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtPathPortable = new System.Windows.Forms.TextBox();
            this.numThread = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.ckbRandomGroup = new System.Windows.Forms.CheckBox();
            this.numGroupSwitch = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.ckbLapLai = new System.Windows.Forms.CheckBox();
            this.ckbRandomPost = new System.Windows.Forms.CheckBox();
            this.numPauseTime = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.numPausePost = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.numTimePostTo = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.numTimePostFrom = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.lblFailClone = new System.Windows.Forms.Label();
            this.lblTongClone = new System.Windows.Forms.Label();
            this.lblOkClone = new System.Windows.Forms.Label();
            this.btnThemClone = new iTalk.iTalk_Button_2();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.iTalk_GroupBox2 = new iTalk.iTalk_GroupBox();
            this.lstListPost = new iTalk.iTalk_Listview();
            this.btnSuaBaiDang = new iTalk.iTalk_Button_2();
            this.btnThemBaiDang = new iTalk.iTalk_Button_2();
            this.btnXoaBaiDang = new iTalk.iTalk_Button_1();
            this.iTalk_GroupBox1 = new iTalk.iTalk_GroupBox();
            this.txtNoiDung = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTenLuuBaiDang = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAnh = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtViTri = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtGia = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMatHang = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.iTalk_GroupBox4.SuspendLayout();
            this.iTalk_GroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numThread)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGroupSwitch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPauseTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPausePost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTimePostTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTimePostFrom)).BeginInit();
            this.iTalk_GroupBox2.SuspendLayout();
            this.iTalk_GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLoger
            // 
            this.btnLoger.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoger.BackColor = System.Drawing.Color.Transparent;
            this.btnLoger.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnLoger.Image = null;
            this.btnLoger.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLoger.Location = new System.Drawing.Point(558, 495);
            this.btnLoger.Name = "btnLoger";
            this.btnLoger.Size = new System.Drawing.Size(117, 20);
            this.btnLoger.TabIndex = 17;
            this.btnLoger.Text = "Hiện Loger >>";
            this.btnLoger.TextAlignment = System.Drawing.StringAlignment.Center;
            this.btnLoger.Click += new System.EventHandler(this.btnLoger_Click);
            // 
            // iTalk_GroupBox4
            // 
            this.iTalk_GroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.iTalk_GroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.iTalk_GroupBox4.Controls.Add(this.lstLoger);
            this.iTalk_GroupBox4.Location = new System.Drawing.Point(685, 12);
            this.iTalk_GroupBox4.MinimumSize = new System.Drawing.Size(136, 50);
            this.iTalk_GroupBox4.Name = "iTalk_GroupBox4";
            this.iTalk_GroupBox4.Padding = new System.Windows.Forms.Padding(5, 28, 5, 5);
            this.iTalk_GroupBox4.Size = new System.Drawing.Size(299, 503);
            this.iTalk_GroupBox4.TabIndex = 16;
            this.iTalk_GroupBox4.Text = "Lịch sử đăng bài";
            // 
            // lstLoger
            // 
            this.lstLoger.BackColor = System.Drawing.SystemColors.Window;
            this.lstLoger.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstLoger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstLoger.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstLoger.HideSelection = false;
            this.lstLoger.Location = new System.Drawing.Point(5, 28);
            this.lstLoger.Name = "lstLoger";
            this.lstLoger.Size = new System.Drawing.Size(289, 470);
            this.lstLoger.TabIndex = 14;
            this.lstLoger.UseCompatibleStateImageBehavior = false;
            // 
            // btnStart
            // 
            this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStart.BackColor = System.Drawing.Color.Transparent;
            this.btnStart.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.btnStart.ForeColor = System.Drawing.Color.White;
            this.btnStart.Image = null;
            this.btnStart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStart.Location = new System.Drawing.Point(70, 475);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(166, 40);
            this.btnStart.TabIndex = 4;
            this.btnStart.Text = "Bắt Đầu";
            this.btnStart.TextAlignment = System.Drawing.StringAlignment.Center;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStop.BackColor = System.Drawing.Color.Transparent;
            this.btnStop.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnStop.Image = null;
            this.btnStop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStop.Location = new System.Drawing.Point(259, 475);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(166, 40);
            this.btnStop.TabIndex = 3;
            this.btnStop.Text = "Kết Thúc";
            this.btnStop.TextAlignment = System.Drawing.StringAlignment.Center;
            this.btnStop.Visible = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // iTalk_GroupBox3
            // 
            this.iTalk_GroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.iTalk_GroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.iTalk_GroupBox3.Controls.Add(this.label18);
            this.iTalk_GroupBox3.Controls.Add(this.txtPathPortable);
            this.iTalk_GroupBox3.Controls.Add(this.numThread);
            this.iTalk_GroupBox3.Controls.Add(this.label17);
            this.iTalk_GroupBox3.Controls.Add(this.ckbRandomGroup);
            this.iTalk_GroupBox3.Controls.Add(this.numGroupSwitch);
            this.iTalk_GroupBox3.Controls.Add(this.label16);
            this.iTalk_GroupBox3.Controls.Add(this.ckbLapLai);
            this.iTalk_GroupBox3.Controls.Add(this.ckbRandomPost);
            this.iTalk_GroupBox3.Controls.Add(this.numPauseTime);
            this.iTalk_GroupBox3.Controls.Add(this.label15);
            this.iTalk_GroupBox3.Controls.Add(this.numPausePost);
            this.iTalk_GroupBox3.Controls.Add(this.label14);
            this.iTalk_GroupBox3.Controls.Add(this.numTimePostTo);
            this.iTalk_GroupBox3.Controls.Add(this.label13);
            this.iTalk_GroupBox3.Controls.Add(this.numTimePostFrom);
            this.iTalk_GroupBox3.Controls.Add(this.label12);
            this.iTalk_GroupBox3.Controls.Add(this.lblFailClone);
            this.iTalk_GroupBox3.Controls.Add(this.lblTongClone);
            this.iTalk_GroupBox3.Controls.Add(this.lblOkClone);
            this.iTalk_GroupBox3.Controls.Add(this.btnThemClone);
            this.iTalk_GroupBox3.Controls.Add(this.label8);
            this.iTalk_GroupBox3.Controls.Add(this.label7);
            this.iTalk_GroupBox3.Controls.Add(this.label6);
            this.iTalk_GroupBox3.Location = new System.Drawing.Point(12, 302);
            this.iTalk_GroupBox3.MinimumSize = new System.Drawing.Size(136, 50);
            this.iTalk_GroupBox3.Name = "iTalk_GroupBox3";
            this.iTalk_GroupBox3.Padding = new System.Windows.Forms.Padding(5, 28, 5, 5);
            this.iTalk_GroupBox3.Size = new System.Drawing.Size(663, 167);
            this.iTalk_GroupBox3.TabIndex = 2;
            this.iTalk_GroupBox3.Text = "Cài đặt tự động đăng tin";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 139);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(160, 13);
            this.label18.TabIndex = 25;
            this.label18.Text = "Thư mục chứa Chrome Portable:";
            // 
            // txtPathPortable
            // 
            this.txtPathPortable.Location = new System.Drawing.Point(203, 136);
            this.txtPathPortable.MaxLength = 999999999;
            this.txtPathPortable.Name = "txtPathPortable";
            this.txtPathPortable.ReadOnly = true;
            this.txtPathPortable.Size = new System.Drawing.Size(418, 20);
            this.txtPathPortable.TabIndex = 24;
            this.txtPathPortable.Text = "Nháy đúp để chọn";
            this.txtPathPortable.TextChanged += new System.EventHandler(this.txtPathPortable_TextChanged);
            this.txtPathPortable.DoubleClick += new System.EventHandler(this.txtPathPortable_DoubleClick);
            // 
            // numThread
            // 
            this.numThread.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.numThread.Location = new System.Drawing.Point(577, 26);
            this.numThread.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numThread.Name = "numThread";
            this.numThread.Size = new System.Drawing.Size(44, 20);
            this.numThread.TabIndex = 22;
            this.numThread.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(519, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 13);
            this.label17.TabIndex = 21;
            this.label17.Text = "Số luồng:";
            // 
            // ckbRandomGroup
            // 
            this.ckbRandomGroup.AutoSize = true;
            this.ckbRandomGroup.Location = new System.Drawing.Point(469, 75);
            this.ckbRandomGroup.Name = "ckbRandomGroup";
            this.ckbRandomGroup.Size = new System.Drawing.Size(136, 17);
            this.ckbRandomGroup.TabIndex = 20;
            this.ckbRandomGroup.Text = "Chọn nhóm ngẫu nhiên";
            this.ckbRandomGroup.UseVisualStyleBackColor = true;
            // 
            // numGroupSwitch
            // 
            this.numGroupSwitch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.numGroupSwitch.Location = new System.Drawing.Point(405, 74);
            this.numGroupSwitch.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numGroupSwitch.Name = "numGroupSwitch";
            this.numGroupSwitch.Size = new System.Drawing.Size(44, 20);
            this.numGroupSwitch.TabIndex = 19;
            this.numGroupSwitch.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(200, 77);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(200, 13);
            this.label16.TabIndex = 18;
            this.label16.Text = "Chuyển clone sau khi đăng được (nhóm)";
            // 
            // ckbLapLai
            // 
            this.ckbLapLai.AutoSize = true;
            this.ckbLapLai.Location = new System.Drawing.Point(469, 102);
            this.ckbLapLai.Name = "ckbLapLai";
            this.ckbLapLai.Size = new System.Drawing.Size(141, 17);
            this.ckbLapLai.TabIndex = 17;
            this.ckbLapLai.Text = "Lặp lại sau khi hết clone";
            this.ckbLapLai.UseVisualStyleBackColor = true;
            // 
            // ckbRandomPost
            // 
            this.ckbRandomPost.AutoSize = true;
            this.ckbRandomPost.Location = new System.Drawing.Point(203, 102);
            this.ckbRandomPost.Name = "ckbRandomPost";
            this.ckbRandomPost.Size = new System.Drawing.Size(235, 17);
            this.ckbRandomPost.TabIndex = 16;
            this.ckbRandomPost.Text = "Đăng bài ngẫu nhiên theo danh sách đã lưu";
            this.ckbRandomPost.UseVisualStyleBackColor = true;
            // 
            // numPauseTime
            // 
            this.numPauseTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.numPauseTime.Location = new System.Drawing.Point(577, 50);
            this.numPauseTime.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numPauseTime.Name = "numPauseTime";
            this.numPauseTime.Size = new System.Drawing.Size(44, 20);
            this.numPauseTime.TabIndex = 15;
            this.numPauseTime.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(440, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(131, 13);
            this.label15.TabIndex = 14;
            this.label15.Text = "Thời gian tạm dừng (phút):";
            // 
            // numPausePost
            // 
            this.numPausePost.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.numPausePost.Location = new System.Drawing.Point(380, 50);
            this.numPausePost.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numPausePost.Name = "numPausePost";
            this.numPausePost.Size = new System.Drawing.Size(44, 20);
            this.numPausePost.TabIndex = 13;
            this.numPausePost.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(200, 52);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(174, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "Tạm dừng sau khi đăng được (bài):";
            // 
            // numTimePostTo
            // 
            this.numTimePostTo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.numTimePostTo.Location = new System.Drawing.Point(415, 26);
            this.numTimePostTo.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numTimePostTo.Name = "numTimePostTo";
            this.numTimePostTo.Size = new System.Drawing.Size(44, 20);
            this.numTimePostTo.TabIndex = 11;
            this.numTimePostTo.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(383, 28);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(26, 13);
            this.label13.TabIndex = 10;
            this.label13.Text = "đến";
            // 
            // numTimePostFrom
            // 
            this.numTimePostFrom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.numTimePostFrom.Location = new System.Drawing.Point(333, 26);
            this.numTimePostFrom.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numTimePostFrom.Name = "numTimePostFrom";
            this.numTimePostFrom.Size = new System.Drawing.Size(44, 20);
            this.numTimePostFrom.TabIndex = 9;
            this.numTimePostFrom.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(200, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(127, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Thời gian đăng bài (giây):";
            // 
            // lblFailClone
            // 
            this.lblFailClone.AutoSize = true;
            this.lblFailClone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblFailClone.Location = new System.Drawing.Point(106, 77);
            this.lblFailClone.Name = "lblFailClone";
            this.lblFailClone.Size = new System.Drawing.Size(13, 13);
            this.lblFailClone.TabIndex = 7;
            this.lblFailClone.Text = "0";
            // 
            // lblTongClone
            // 
            this.lblTongClone.AutoSize = true;
            this.lblTongClone.Location = new System.Drawing.Point(106, 28);
            this.lblTongClone.Name = "lblTongClone";
            this.lblTongClone.Size = new System.Drawing.Size(13, 13);
            this.lblTongClone.TabIndex = 6;
            this.lblTongClone.Text = "0";
            // 
            // lblOkClone
            // 
            this.lblOkClone.AutoSize = true;
            this.lblOkClone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblOkClone.Location = new System.Drawing.Point(106, 52);
            this.lblOkClone.Name = "lblOkClone";
            this.lblOkClone.Size = new System.Drawing.Size(13, 13);
            this.lblOkClone.TabIndex = 5;
            this.lblOkClone.Text = "0";
            // 
            // btnThemClone
            // 
            this.btnThemClone.BackColor = System.Drawing.Color.Transparent;
            this.btnThemClone.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.btnThemClone.ForeColor = System.Drawing.Color.White;
            this.btnThemClone.Image = null;
            this.btnThemClone.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnThemClone.Location = new System.Drawing.Point(19, 102);
            this.btnThemClone.Name = "btnThemClone";
            this.btnThemClone.Size = new System.Drawing.Size(130, 26);
            this.btnThemClone.TabIndex = 4;
            this.btnThemClone.Text = "Thêm clone";
            this.btnThemClone.TextAlignment = System.Drawing.StringAlignment.Center;
            this.btnThemClone.Click += new System.EventHandler(this.btnThemClone_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(16, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Clone thất bại:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(16, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Clone thành công:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Clone còn lại:";
            // 
            // iTalk_GroupBox2
            // 
            this.iTalk_GroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.iTalk_GroupBox2.Controls.Add(this.lstListPost);
            this.iTalk_GroupBox2.Controls.Add(this.btnSuaBaiDang);
            this.iTalk_GroupBox2.Controls.Add(this.btnThemBaiDang);
            this.iTalk_GroupBox2.Controls.Add(this.btnXoaBaiDang);
            this.iTalk_GroupBox2.Location = new System.Drawing.Point(459, 12);
            this.iTalk_GroupBox2.MinimumSize = new System.Drawing.Size(136, 50);
            this.iTalk_GroupBox2.Name = "iTalk_GroupBox2";
            this.iTalk_GroupBox2.Padding = new System.Windows.Forms.Padding(5, 28, 5, 5);
            this.iTalk_GroupBox2.Size = new System.Drawing.Size(216, 284);
            this.iTalk_GroupBox2.TabIndex = 1;
            this.iTalk_GroupBox2.Text = "Danh sách bài đăng";
            // 
            // lstListPost
            // 
            this.lstListPost.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.lstListPost.BackColor = System.Drawing.SystemColors.Window;
            this.lstListPost.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstListPost.CheckBoxes = true;
            this.lstListPost.FullRowSelect = true;
            this.lstListPost.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lstListPost.HideSelection = false;
            this.lstListPost.Location = new System.Drawing.Point(8, 28);
            this.lstListPost.Name = "lstListPost";
            this.lstListPost.OwnerDraw = true;
            this.lstListPost.Size = new System.Drawing.Size(200, 217);
            this.lstListPost.TabIndex = 15;
            this.lstListPost.UseCompatibleStateImageBehavior = false;
            this.lstListPost.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstListPost_ColumnClick);
            this.lstListPost.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lstListPost_DrawColumnHeader);
            this.lstListPost.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lstListPost_DrawItem);
            this.lstListPost.DrawSubItem += new System.Windows.Forms.DrawListViewSubItemEventHandler(this.lstListPost_DrawSubItem);
            this.lstListPost.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstListPost_MouseDoubleClick);
            // 
            // btnSuaBaiDang
            // 
            this.btnSuaBaiDang.BackColor = System.Drawing.Color.Transparent;
            this.btnSuaBaiDang.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.btnSuaBaiDang.ForeColor = System.Drawing.Color.White;
            this.btnSuaBaiDang.Image = null;
            this.btnSuaBaiDang.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSuaBaiDang.Location = new System.Drawing.Point(76, 250);
            this.btnSuaBaiDang.Name = "btnSuaBaiDang";
            this.btnSuaBaiDang.Size = new System.Drawing.Size(64, 26);
            this.btnSuaBaiDang.TabIndex = 4;
            this.btnSuaBaiDang.Text = "Sửa";
            this.btnSuaBaiDang.TextAlignment = System.Drawing.StringAlignment.Center;
            this.btnSuaBaiDang.Click += new System.EventHandler(this.btnSuaBaiDang_Click);
            // 
            // btnThemBaiDang
            // 
            this.btnThemBaiDang.BackColor = System.Drawing.Color.Transparent;
            this.btnThemBaiDang.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.btnThemBaiDang.ForeColor = System.Drawing.Color.White;
            this.btnThemBaiDang.Image = null;
            this.btnThemBaiDang.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnThemBaiDang.Location = new System.Drawing.Point(7, 250);
            this.btnThemBaiDang.Name = "btnThemBaiDang";
            this.btnThemBaiDang.Size = new System.Drawing.Size(64, 26);
            this.btnThemBaiDang.TabIndex = 1;
            this.btnThemBaiDang.Text = "Thêm";
            this.btnThemBaiDang.TextAlignment = System.Drawing.StringAlignment.Center;
            this.btnThemBaiDang.Click += new System.EventHandler(this.btnThemBaiDang_Click);
            // 
            // btnXoaBaiDang
            // 
            this.btnXoaBaiDang.BackColor = System.Drawing.Color.Transparent;
            this.btnXoaBaiDang.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnXoaBaiDang.Image = null;
            this.btnXoaBaiDang.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnXoaBaiDang.Location = new System.Drawing.Point(145, 249);
            this.btnXoaBaiDang.Name = "btnXoaBaiDang";
            this.btnXoaBaiDang.Size = new System.Drawing.Size(64, 26);
            this.btnXoaBaiDang.TabIndex = 0;
            this.btnXoaBaiDang.Text = "Xóa";
            this.btnXoaBaiDang.TextAlignment = System.Drawing.StringAlignment.Center;
            this.btnXoaBaiDang.Click += new System.EventHandler(this.btnXoaBaiDang_Click);
            // 
            // iTalk_GroupBox1
            // 
            this.iTalk_GroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.iTalk_GroupBox1.Controls.Add(this.txtNoiDung);
            this.iTalk_GroupBox1.Controls.Add(this.label11);
            this.iTalk_GroupBox1.Controls.Add(this.txtTenLuuBaiDang);
            this.iTalk_GroupBox1.Controls.Add(this.label9);
            this.iTalk_GroupBox1.Controls.Add(this.txtAnh);
            this.iTalk_GroupBox1.Controls.Add(this.label5);
            this.iTalk_GroupBox1.Controls.Add(this.label4);
            this.iTalk_GroupBox1.Controls.Add(this.txtViTri);
            this.iTalk_GroupBox1.Controls.Add(this.label3);
            this.iTalk_GroupBox1.Controls.Add(this.txtGia);
            this.iTalk_GroupBox1.Controls.Add(this.label2);
            this.iTalk_GroupBox1.Controls.Add(this.txtMatHang);
            this.iTalk_GroupBox1.Controls.Add(this.label1);
            this.iTalk_GroupBox1.Location = new System.Drawing.Point(12, 12);
            this.iTalk_GroupBox1.MinimumSize = new System.Drawing.Size(136, 50);
            this.iTalk_GroupBox1.Name = "iTalk_GroupBox1";
            this.iTalk_GroupBox1.Padding = new System.Windows.Forms.Padding(5, 28, 5, 5);
            this.iTalk_GroupBox1.Size = new System.Drawing.Size(441, 284);
            this.iTalk_GroupBox1.TabIndex = 0;
            this.iTalk_GroupBox1.Text = "Nội dung bài đăng";
            // 
            // txtNoiDung
            // 
            this.txtNoiDung.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNoiDung.Location = new System.Drawing.Point(69, 100);
            this.txtNoiDung.Name = "txtNoiDung";
            this.txtNoiDung.Size = new System.Drawing.Size(364, 91);
            this.txtNoiDung.TabIndex = 13;
            this.txtNoiDung.Text = "";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(66, 194);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(275, 26);
            this.label11.TabIndex = 12;
            this.label11.Text = "Spin: [d] -> số, [w] -> chữ, [r] -> icon, [s] -> ký tự đặc biệt.\r\n          {a|b|" +
    "c|d} -> random lấy 1 phần tử.";
            // 
            // txtTenLuuBaiDang
            // 
            this.txtTenLuuBaiDang.Location = new System.Drawing.Point(161, 255);
            this.txtTenLuuBaiDang.Name = "txtTenLuuBaiDang";
            this.txtTenLuuBaiDang.Size = new System.Drawing.Size(155, 20);
            this.txtTenLuuBaiDang.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(89, 258);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Tên gợi nhớ:";
            // 
            // txtAnh
            // 
            this.txtAnh.Location = new System.Drawing.Point(69, 229);
            this.txtAnh.MaxLength = 999999999;
            this.txtAnh.Name = "txtAnh";
            this.txtAnh.ReadOnly = true;
            this.txtAnh.Size = new System.Drawing.Size(364, 20);
            this.txtAnh.TabIndex = 9;
            this.txtAnh.Text = "Nháy đúp để chọn ảnh";
            this.txtAnh.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.txtAnh_MouseDoubleClick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 232);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Ảnh:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Nội dung:";
            // 
            // txtViTri
            // 
            this.txtViTri.Location = new System.Drawing.Point(69, 77);
            this.txtViTri.Name = "txtViTri";
            this.txtViTri.Size = new System.Drawing.Size(364, 20);
            this.txtViTri.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Vị trí:";
            // 
            // txtGia
            // 
            this.txtGia.Location = new System.Drawing.Point(69, 53);
            this.txtGia.Name = "txtGia";
            this.txtGia.Size = new System.Drawing.Size(155, 20);
            this.txtGia.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Giá:";
            // 
            // txtMatHang
            // 
            this.txtMatHang.Location = new System.Drawing.Point(69, 30);
            this.txtMatHang.Name = "txtMatHang";
            this.txtMatHang.Size = new System.Drawing.Size(364, 20);
            this.txtMatHang.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mặt hàng:";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 548);
            this.Controls.Add(this.btnLoger);
            this.Controls.Add(this.iTalk_GroupBox4);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.iTalk_GroupBox3);
            this.Controls.Add(this.iTalk_GroupBox2);
            this.Controls.Add(this.iTalk_GroupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phần mềm đăng bài bán hàng lên nhóm facebook - V1.0";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.iTalk_GroupBox4.ResumeLayout(false);
            this.iTalk_GroupBox3.ResumeLayout(false);
            this.iTalk_GroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numThread)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGroupSwitch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPauseTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPausePost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTimePostTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTimePostFrom)).EndInit();
            this.iTalk_GroupBox2.ResumeLayout(false);
            this.iTalk_GroupBox1.ResumeLayout(false);
            this.iTalk_GroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private iTalk.iTalk_GroupBox iTalk_GroupBox1;
        private System.Windows.Forms.TextBox txtAnh;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtViTri;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtGia;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMatHang;
        private System.Windows.Forms.Label label1;
        private iTalk.iTalk_GroupBox iTalk_GroupBox2;
        private iTalk.iTalk_Button_2 btnThemBaiDang;
        private iTalk.iTalk_Button_1 btnXoaBaiDang;
        private iTalk.iTalk_GroupBox iTalk_GroupBox3;
        private System.Windows.Forms.Label lblFailClone;
        private System.Windows.Forms.Label lblTongClone;
        private System.Windows.Forms.Label lblOkClone;
        private iTalk.iTalk_Button_2 btnThemClone;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numPauseTime;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown numPausePost;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown numTimePostTo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown numTimePostFrom;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox ckbRandomPost;
        private iTalk.iTalk_Button_1 btnStop;
        private iTalk.iTalk_Button_2 btnStart;
        private System.Windows.Forms.NumericUpDown numGroupSwitch;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox ckbLapLai;
        private System.Windows.Forms.CheckBox ckbRandomGroup;
        private System.Windows.Forms.NumericUpDown numThread;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtTenLuuBaiDang;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RichTextBox txtNoiDung;
        private System.Windows.Forms.Label label11;
        private iTalk.iTalk_Button_2 btnSuaBaiDang;
        private iTalk.iTalk_GroupBox iTalk_GroupBox4;
        private iTalk.iTalk_Listview lstLoger;
        private iTalk.iTalk_Button_1 btnLoger;
        private iTalk.iTalk_Listview lstListPost;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtPathPortable;
    }
}

