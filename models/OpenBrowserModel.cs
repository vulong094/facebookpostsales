﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;

namespace FacebookPostSales.models
{
    public class OpenBrowserModel
    {
        public static FirefoxDriver OpenFirefoxPortable(int port, string pathFolder)
        {
            FirefoxDriverService driverService = FirefoxDriverService.CreateDefaultService();
            driverService.HideCommandPromptWindow = true;
            FirefoxOptions options = new FirefoxOptions();

            options.Profile = new FirefoxProfile();
            options.Profile.SetPreference("Browser.safebrowsing.enabled", true);
            options.Profile.SetPreference("dom.webnotifications.enabled", false);
            //options.Profile.AddExtension(Directory.GetCurrentDirectory() + "/imacros_for_firefox-8.9.7-fx.xpi");
            if (port != 0)
            {
                var proxy = new Proxy();
                proxy.SocksProxy = "127.0.0.1:" + port;
                options.Profile.SetProxyPreferences(proxy);
            }
            options.BrowserExecutableLocation = pathFolder + "\\App\\Firefox64\\firefox.exe";
            FirefoxDriver driver = null;
            while (true)
            {
                try
                {
                    driver = new FirefoxDriver(driverService, options, new TimeSpan(0, 5, 0));
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    //throw;
                }
            }

            //try
            //{
            //    driver.Manage().Window.Maximize();
            //}
            //catch
            //{
            //    //
            //}

            return driver;
        }

        public static FirefoxDriver OpenFirefox(int port)
        {
            var fireDriverService = FirefoxDriverService.CreateDefaultService();
            fireDriverService.HideCommandPromptWindow = true;

            FirefoxOptions option = new FirefoxOptions();
            option.Profile = new FirefoxProfile("Firefox");
            option.Profile.SetPreference("Browser.safebrowsing.enabled", true);

            if (port != 0)
            {
                var proxy = new Proxy();
                proxy.SocksProxy = "127.0.0.1:" + port;
                option.Profile.SetProxyPreferences(proxy);
            }

            FirefoxDriver driver = new FirefoxDriver(fireDriverService, option);
            driver.Manage().Window.Size = new Size(400, 700);
            //driver.Manage().Timeouts().PageLoad = new TimeSpan(0, 5, 0);
            return driver;
        }

        public static ChromeDriver ChromePortable(string uid, int port = 0, bool createPortable = false, bool hideBrowser = false)
        {
            ChromeDriverService chromeDriverService = ChromeDriverService.CreateDefaultService();
            chromeDriverService.HideCommandPromptWindow = true;

            ChromeOptions chromeOption = new ChromeOptions();
            if (createPortable)
            {
                //string[] name = path.Split('\\');
                chromeOption.AddArgument("--user-data-dir=" + frmMain.pathPortable + "\\" + uid);
                //chromeOption.AddArgument("--user-data-dir=" + path.Replace("\\" + name[name.Length - 1], ""));
                //chromeOption.AddArgument("--profile-directory=" + name[name.Length - 1]);
            }

            //chromeOption.AddArgument("--incognito");
            if (hideBrowser)
            {
                chromeOption.AddArgument("--headless");
            }
            chromeOption.AddArguments("--disable-web-security");
            chromeOption.AddArguments("--disable-rtc-smoothness-algorithm");
            chromeOption.AddArguments("--disable-webrtc-hw-decoding");
            chromeOption.AddArguments("--disable-webrtc-hw-encoding");
            chromeOption.AddArguments("--disable-webrtc-multiple-routes");
            chromeOption.AddArguments("--disable-webrtc-hw-vp8-encoding");
            chromeOption.AddArguments("--enforce-webrtc-ip-permission-check");
            chromeOption.AddArguments("--force-webrtc-ip-handling-policy");
            chromeOption.AddArguments("ignore-certificate-errors");
            chromeOption.AddArguments("disable-infobars");
            chromeOption.AddArguments("mute-audio");
            chromeOption.AddArguments("--disable-popup-blocking");
            chromeOption.AddArguments("--disable-plugins");
            //chromeOption.AddArguments("--window-size=400,700");
            chromeOption.AddArguments("--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0");
            if (port != 0)
            {
                chromeOption.AddArguments("--proxy-server=socks5://127.0.0.1:" + port);
            }

            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.notifications", 2);
            //chromeOption.AddUserProfilePreference("profile.default_content_setting_values.javascript", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.plugins", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.popups", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.geolocation", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.auto_select_certificate", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.mixed_script", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.media_stream", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.media_stream_mic", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.media_stream_camera", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.protocol_handlers", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.midi_sysex", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.push_messaging", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.ssl_cert_decisions", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.metro_switch_to_desktop", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.protected_media_identifier", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.site_engagement", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.durable_storage", 2);
            chromeOption.AddUserProfilePreference("profile.managed_default_content_settings.images", 2);
            chromeOption.AddUserProfilePreference("useAutomationExtension", false);
            ChromeDriver driver = new ChromeDriver(chromeDriverService, chromeOption);

            return driver;
        }

        public static ChromeDriver OpenChrome(string ua, int port, bool hideBrowser = false)
        {
            ChromeDriverService chromeDriverService = ChromeDriverService.CreateDefaultService();
            chromeDriverService.HideCommandPromptWindow = true;

            ChromeOptions chromeOption = new ChromeOptions();
            chromeOption.AddArgument("--incognito");
            if (hideBrowser)
            {
                chromeOption.AddArgument("--headless");
            }
            chromeOption.AddArguments("--disable-web-security");
            chromeOption.AddArguments("--disable-rtc-smoothness-algorithm");
            chromeOption.AddArguments("--disable-webrtc-hw-decoding");
            chromeOption.AddArguments("--disable-webrtc-hw-encoding");
            chromeOption.AddArguments("--disable-webrtc-multiple-routes");
            chromeOption.AddArguments("--disable-webrtc-hw-vp8-encoding");
            chromeOption.AddArguments("--enforce-webrtc-ip-permission-check");
            chromeOption.AddArguments("--force-webrtc-ip-handling-policy");
            chromeOption.AddArguments("ignore-certificate-errors");
            chromeOption.AddArguments("disable-infobars");
            chromeOption.AddArguments("mute-audio");
            chromeOption.AddArguments("--disable-popup-blocking");
            chromeOption.AddArguments("--disable-plugins");
            //chromeOption.AddArguments("--window-size=400,700");
            if (ua == "")
            {
                chromeOption.AddArguments("--user-agent=Mozilla/5.0 (iPhone; CPU iPhone OS 8_3 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) FxiOS/40.0 Mobile/12F69 Safari/600.1.4");
            }
            else
            {
                chromeOption.AddArguments("--user-agent=" + ua);
            }

            if (port != 0)
            {
                chromeOption.AddArguments("--proxy-server=socks5://127.0.0.1:" + port);
            }

            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.notifications", 2);
            //chromeOption.AddUserProfilePreference("profile.default_content_setting_values.javascript", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.plugins", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.popups", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.geolocation", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.auto_select_certificate", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.mixed_script", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.media_stream", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.media_stream_mic", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.media_stream_camera", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.protocol_handlers", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.midi_sysex", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.push_messaging", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.ssl_cert_decisions", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.metro_switch_to_desktop", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.protected_media_identifier", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.site_engagement", 2);
            chromeOption.AddUserProfilePreference("profile.default_content_setting_values.durable_storage", 2);
            chromeOption.AddUserProfilePreference("profile.managed_default_content_settings.images", 2);
            chromeOption.AddUserProfilePreference("useAutomationExtension", false);
            ChromeDriver driver = new ChromeDriver(chromeDriverService, chromeOption);

            return driver;
        }

        public static IWebDriver OpenNewTab(IWebDriver driver, string url)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var windowHandles = driver.WindowHandles;
            js.ExecuteScript(string.Format("window.open('{0}', '_blank');", url));
            var newWindowHandles = driver.WindowHandles;
            var openedWindowHandle = newWindowHandles.Except(windowHandles).Single();
            driver.SwitchTo().Window(openedWindowHandle);
            return driver;
        }

        public static IWebDriver LoginCookie(IWebDriver driver, string cookie)
        {
            driver.Url = "https://www.facebook.com/";
            string[] cookies = cookie.Split(';');
            foreach (string s in cookies)
            {
                string[] ck = s.Split('=');
                driver.Manage().Cookies.AddCookie(new Cookie(ck[0].Trim(), ck[1].Trim()));
            }
            driver.Navigate().Refresh();
            return driver;
        }

        public static void ExtractToDirectory(int numberThread)
        {
            var dirs = Directory.GetDirectories("Profile");
            for (int i = dirs.Length; i < numberThread; i++)
            {
                if (!Directory.Exists(Directory.GetCurrentDirectory() + $"/Profile/Profile {i}"))
                {
                    Directory.CreateDirectory(Directory.GetCurrentDirectory() + $"/Profile/Profile {i}");
                }
                ZipFile.ExtractToDirectory(Directory.GetCurrentDirectory() + "/Profile/Profile.zip", Directory.GetCurrentDirectory() + $"/Profile/Profile {i}");
            }
        }
    }
}
