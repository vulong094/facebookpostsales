﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacebookPostSales.dtos;
using FacebookPostSales.helpers;
using FacebookPostSales.values;
using FindElementbyLong;
using Mono.Web;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using xNet;
using Keys = OpenQA.Selenium.Keys;

namespace FacebookPostSales.models
{
    class OneThreadModel
    {
        private IWebDriver driver;
        private string cookie = "", clone = "";
        private ConfigDto configDto;
        FindElement element = new FindElement();
        FindElement _element = new FindElement();
        List<string> groupIdList = new List<string>();
        ReturnResult Result = new ReturnResult();
        private Form frmMain;
        private ListView lstListView;

        public OneThreadModel(Form _frmMain, ListView lstLoger, IWebDriver _driver, string _clone, string _cookie, ConfigDto _configDto)
        {
            lstListView = lstLoger;
            frmMain = _frmMain;
            driver = _driver;
            cookie = _cookie;
            clone = _clone;
            configDto = _configDto;
            Result.CookieSuccess = new List<string>();
            Result.CookiePending = new List<string>();
            Result.CookieSpam = new List<string>();
        }

        public ReturnResult Start(List<BaiDangDto> baiDangDtos)
        {
            //string[] acc = clone.Split('|');
            Result.Uid = clone;
            try
            {
                driver.Url = "https://www.facebook.com/";
            }
            catch
            {
                Result.Status = ReturnStatus.FAIL;
                return Result;
            }
            Thread.Sleep(3000);

            //AddCookieToBrowser(cookie, driver);
            //driver.Navigate().Refresh();

            //_element.SendKeyToElement(driver, "Id", "email", 0, acc[0]);
            //driver.FindElementById("pass").SendKeys(acc[1] + OpenQA.Selenium.Keys.Enter);
            //Thread.Sleep(10000);
            try
            {
                driver.FindElement(By.Id("email")).SendKeys("email");
                Result.Status = ReturnStatus.COOKIE_DIE;
                FunctionHelper.AddItemToListView(lstListView, Result.Uid, "Cookie Error");
                return Result;
            }
            catch
            {
                //
            }

            if (driver.Url.Contains("login"))
            {
                Thread.Sleep(5000);
                Result.Status = ReturnStatus.COOKIE_DIE;
                FunctionHelper.AddItemToListView(lstListView, Result.Uid, "Uid Error");
                return Result;
            }
            if (driver.Url.Contains("checkpoint"))
            {
                Thread.Sleep(5000);
                Result.Status = ReturnStatus.CHECKPOINT;
                FunctionHelper.AddItemToListView(lstListView, Result.Uid, "CHECKPOINT");
                return Result;
            }

            var cookies = driver.Manage().Cookies.AllCookies;
            foreach (Cookie cookie1 in cookies)
            {
                cookie += $"{cookie1.Name}={cookie1.Value};";
            }

            //try
            //{
            //    driver.FindElementByName("email").SendKeys("Test");
            //    Result.Status = ReturnStatus.COOKIE_DIE;
            //    return Result;
            //}
            //catch { }

            try
            {
                HttpRequest request = new HttpRequest();
                request.AddHeader("Cookie", cookie);
                request.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36");
                string body = request.Get("https://mbasic.facebook.com/groups/?seemore").ToString();

                MatchCollection collection = Regex.Matches(body, "<a href=\"\\/groups\\/(.*?)\\?refid=27\">");
                for (int i = 1; i < collection.Count; i++)
                {
                    groupIdList.Add(collection[i].Groups[1].Value);
                }
            }
            catch
            {
                Console.WriteLine("error get list group");
                Result.Status = ReturnStatus.FAIL;
                return Result;
            }

            string id = "";
            int sum = 0, count = 0;
            if (configDto.NumberGroup > groupIdList.Count)
            {
                count = groupIdList.Count;
            }
            else
            {
                count = configDto.NumberGroup;
            }
            //Số group đăng được, sẽ chuyển clone khác
            BaiDangDto baiDangDto = null;
            for (int i = 0; i < count; i++)
            {
                id = groupIdList[new Random().Next(groupIdList.Count)];
                groupIdList.Remove(id);

                baiDangDto = baiDangDtos[new Random().Next(baiDangDtos.Count)];

                ReturnStatus status = PostToGroup(id, baiDangDto);
                //while (groupIdList.Count > 0 && status == ReturnStatus.FAIL)
                //{
                //    id = groupIdList[new Random().Next(groupIdList.Count)];
                //    groupIdList.Remove(id);
                //    status = PostToGroup(id, baiDangDto);
                //}

                if (status == ReturnStatus.BLOCK)
                {
                    Result.Status = ReturnStatus.BLOCK;
                    return Result;
                }
                sum++;
                if (sum == configDto.NumberPostPause)
                {
                    //Thời gian tạm nghỉ sau khi đăng được số bài
                    sum = 0;
                    Thread.Sleep(new TimeSpan(0, configDto.TimePostPause, 0));
                }
                else
                {
                    if (sum != configDto.NumberGroup)
                    {
                        //Thời gian nghỉ giữa mỗi bài đăng
                        Thread.Sleep(new TimeSpan(0, 0, new Random().Next(configDto.TimePostFrom, configDto.TimePostTo)));
                    }
                }
            }

            Result.Status = ReturnStatus.SUCCESS;
            return Result;
        }

        private ReturnStatus PostToGroup(string idGroup, BaiDangDto baiDangDto)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            string[] imageStrings = baiDangDto.Anh.Split(',');

            string mathang = baiDangDto.MatHang
                .Replace("[d]", new Random().Next(10).ToString())
                .Replace("[w]", FunctionHelper.RandomString(1))
                .Replace("[s]", FunctionHelper.RandomChara(1))
                .Replace("[r]", FunctionHelper.RandomIcon(1));
            MatchCollection collection = Regex.Matches(mathang, "{(.*?)}");
            foreach (Match match in collection)
            {
                string[] s = match.Groups[1].Value.Split('|');
                mathang = mathang.Replace(match.Value, s[new Random().Next(s.Length)]);
            }

            string noidung = baiDangDto.NoiDung
                .Replace("[d]", new Random().Next(10).ToString())
                .Replace("[w]", FunctionHelper.RandomString(1))
                .Replace("[s]", FunctionHelper.RandomChara(1))
                .Replace("[r]", FunctionHelper.RandomIcon(1));

            collection = Regex.Matches(noidung, "{(.*?)}");
            foreach (Match match in collection)
            {
                string[] s = match.Groups[1].Value.Split('|');
                noidung = noidung.Replace(match.Value, s[new Random().Next(s.Length)]);
            }

            try
            {
                driver.Url = $"https://www.facebook.com/groups/{idGroup}";
                js.ExecuteScript("window.scrollTo(0, 300);");
            }
            catch
            {
                return ReturnStatus.FAIL;
            }

            Thread.Sleep(3000);
            try
            {
                IWebElement webElement = driver.FindElement(By.CssSelector("a[attachmentid='SELL']"));
                Actions builder = new Actions(driver);
                builder
                    .MoveToElement(webElement, 50, 50)
                    .DoubleClick()
                    .Build()
                    .Perform();

                Console.WriteLine("===========================> Group sales");
                Thread.Sleep(7000);
                try
                {
                    driver.FindElement(By.CssSelector("a[action='cancel']")).Click();
                    FunctionHelper.AddItemToListView(lstListView, Result.Uid, "Block");
                    return ReturnStatus.BLOCK;
                }
                catch { }

                driver.FindElement(By.CssSelector("input[placeholder='Bạn đang bán gì?']")).Click();
                if (string.IsNullOrEmpty(mathang.Trim()))
                {
                    driver.FindElement(By.CssSelector("input[placeholder='Bạn đang bán gì?']")).SendKeys("s");
                }
                else
                {
                    driver.FindElement(By.CssSelector("input[placeholder='Bạn đang bán gì?']")).SendKeys(mathang);
                }
                
                Thread.Sleep(5000);

                lock (FacebookPostSales.frmMain.lock_up_image)
                {
                    for (int i = 0; i < imageStrings.Length - 1; i++)
                    {
                        //điền ảnh
                        frmMain.Invoke(new MethodInvoker(() =>
                        {
                            Image image = Image.FromFile(imageStrings[i]);
                            Clipboard.SetImage(image);
                        }));
                        driver.FindElement(By.CssSelector("div[role='textbox']")).Click();
                        driver.FindElement(By.CssSelector("div[role='textbox']")).SendKeys(OpenQA.Selenium.Keys.Control + "v");
                    }
                }
                Thread.Sleep(5000);
                driver.FindElement(By.CssSelector("div[role='textbox']")).SendKeys(noidung);
                Thread.Sleep(5000);
                try
                {
                    driver.FindElement(By.CssSelector("input[placeholder='Thêm vị trí (tùy chọn)']")).Clear();
                    Thread.Sleep(1000);
                    driver.FindElement(By.CssSelector("input[placeholder='Thêm vị trí (tùy chọn)']")).SendKeys(Keys.Control + "a" + Keys.Delete);
                    driver.FindElement(By.CssSelector("input[placeholder='Thêm vị trí (tùy chọn)']")).SendKeys(baiDangDto.ViTri);
                    Thread.Sleep(4000);
                }
                catch
                {
                    //
                }

                driver.FindElement(By.CssSelector("input[placeholder='Giá']")).SendKeys(baiDangDto.Gia);
                Thread.Sleep(4000);

                try
                {
                    if (driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']"))
                            .GetAttribute("disabled") == "true")
                    {
                        driver.FindElement(By.CssSelector("input[placeholder='Bạn đang bán gì?']")).SendKeys(mathang);
                    }
                }
                catch
                {
                    //
                }

                try
                {
                    var element = driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']"));
                    js.ExecuteScript("arguments[0].scrollIntoView();", element);
                    //js.ExecuteScript("window.scrollTo(0, 300);");
                }
                catch { }
                try
                {
                    string tiep = driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']")).GetAttribute("disabled");
                    while (tiep == "true")
                    {
                        Thread.Sleep(5000);
                        tiep = driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']")).GetAttribute("disabled");
                    }

                    driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']")).Click();
                    Thread.Sleep(5000);
                    try
                    {
                        var element = driver.FindElement(By.CssSelector("div[data-testid='react-composer-root']"));
                        js.ExecuteScript("arguments[0].scrollIntoView();", element);
                        //js.ExecuteScript("window.scrollTo(0, 300);");
                    }
                    catch { }
                    try
                    {
                        var elements = driver.FindElements(By.CssSelector("div[role='checkbox']"));
                        for (int i = 0; i < 5; i++)
                        {
                            if (elements[i].GetAttribute("aria-checked") == "false")
                            {
                                elements[i].Click();
                            }
                        }
                    }
                    catch { }
                    Thread.Sleep(3000);
                    driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']")).Click();
                    Thread.Sleep(3000);
                }
                catch { }

                try
                {
                    driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']")).Click();
                    Thread.Sleep(3000);

                    string tiep = driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']")).GetAttribute("disabled");
                    while (tiep == "true")
                    {
                        Thread.Sleep(5000);
                        tiep = driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']")).GetAttribute("disabled");
                    }
                }
                catch { }

                Thread.Sleep(10000);
                try
                {
                    driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']")).Click();
                    Thread.Sleep(3000);
                }
                catch { }

                try
                {
                    driver.FindElement(By.CssSelector("a[action='cancel']")).Click();
                    FunctionHelper.AddItemToListView(lstListView, Result.Uid, "Block");
                    return ReturnStatus.BLOCK;
                }
                catch { }

                //get link
                HttpRequest request = new HttpRequest();
                request.AddHeader("Cookie", cookie);
                request.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36");
                string body = HttpUtility.HtmlDecode(request.Get($"https://mbasic.facebook.com/{Result.Uid}/allactivity?ref=bookmarks").ToString());

                Match match = Regex.Match(body, $"href=\"\\/groups\\/{idGroup}\\?view=permalink&id=(.*?)&ref=bookmarks");
                if (match.Success)
                {
                    if (match.Groups[1].Value.Contains("substory_index"))
                    {
                        Result.CookieSpam.Add($"https://www.facebook.com/{match.Groups[1].Value}/");
                        Console.WriteLine("----------------------> Spam");
                        FunctionHelper.AddItemToListView(lstListView, Result.Uid, "Spam");
                    }
                    else
                    {
                        Result.CookieSuccess.Add($"https://www.facebook.com/{match.Groups[1].Value}/");
                        Console.WriteLine("----------------------> Success");
                        FunctionHelper.AddItemToListView(lstListView, Result.Uid, "Success");
                    }
                }
                else
                {
                    Result.CookiePending.Add($"https://www.facebook.com/groups/{idGroup}/pending");
                    Console.WriteLine("----------------------> Pending");
                    FunctionHelper.AddItemToListView(lstListView, Result.Uid, "Pending");
                }

            }
            catch
            {
                try
                {
                    Console.WriteLine("===========================> Group not sales");
                    IWebElement webElement = driver.FindElement(By.CssSelector("a[attachmentid='STATUS']"));
                    Actions builder = new Actions(driver);
                    builder
                        .MoveToElement(webElement, 50, 50)
                        .DoubleClick()
                        .Build()
                        .Perform();

                    Thread.Sleep(10000);
                    try
                    {
                        driver.FindElement(By.CssSelector("a[action='cancel']")).Click();
                        return ReturnStatus.BLOCK;
                    }
                    catch { }

                    //driver.FindElementByCssSelector("div[data-testid='status-attachment-mentions-input']").Click();
                    //Thread.Sleep(4000);
                    lock (FacebookPostSales.frmMain.lock_up_image)
                    {
                        for (int i = 0; i < imageStrings.Length - 1; i++)
                        {
                            //điền ảnh
                            frmMain.Invoke(new MethodInvoker(() =>
                            {
                                Image image = Image.FromFile(imageStrings[i]);
                                Clipboard.SetImage(image);
                            }));

                            driver.FindElement(By.CssSelector("div[style=\"outline: none; user-select: text; white-space: pre-wrap; overflow-wrap: break-word;\"]")).Click();
                            driver.FindElement(By.CssSelector("div[style=\"outline: none; user-select: text; white-space: pre-wrap; overflow-wrap: break-word;\"]")).SendKeys(Keys.Control + "v");
                            if (i == 0)
                            {
                                Thread.Sleep(5000);
                            }
                        }
                    }
                    Thread.Sleep(5000);
                    driver.FindElement(By.CssSelector("div[style=\"outline: none; user-select: text; white-space: pre-wrap; overflow-wrap: break-word;\"]")).SendKeys(noidung);
                    Thread.Sleep(5000);

                    //try
                    //{
                    //    driver.FindElement(By.CssSelector("input[placeholder='Thêm vị trí (tùy chọn)']")).Clear();
                    //    Thread.Sleep(500);
                    //    driver.FindElement(By.CssSelector("input[placeholder='Thêm vị trí (tùy chọn)']")).SendKeys(baiDangDto.ViTri);
                    //    Thread.Sleep(5000);
                    //    driver.FindElement(By.CssSelector("input[placeholder='Giá']")).Clear();
                    //    Thread.Sleep(500);
                    //    driver.FindElement(By.CssSelector("input[placeholder='Giá']")).SendKeys(baiDangDto.Gia);
                    //    Thread.Sleep(5000);
                    //}
                    //catch { }
                    //try
                    //{
                    //    var element = driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']"));
                    //    js.ExecuteScript("arguments[0].scrollIntoView();", element);
                    //    //js.ExecuteScript("window.scrollTo(0, 300);");
                    //}
                    //catch { }
                    try
                    {
                        string tiep = driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']")).GetAttribute("disabled");
                        while (tiep == "true")
                        {
                            Thread.Sleep(5000);
                            tiep = driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']")).GetAttribute("disabled");
                        }

                        driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']")).Click();
                        Thread.Sleep(2000);
                        driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']")).Click();
                        Thread.Sleep(2000);
                    }
                    catch { }

                    try
                    {
                        driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']")).Click();
                        Thread.Sleep(2000);
                        string tiep = driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']")).GetAttribute("disabled");
                        while (tiep == "true")
                        {
                            Thread.Sleep(5000);
                            tiep = driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']")).GetAttribute("disabled");
                        }
                    }
                    catch { }

                    Thread.Sleep(10000);
                    try
                    {
                        driver.FindElement(By.CssSelector("button[data-testid='react-composer-post-button']")).Click();
                        Thread.Sleep(3000);
                    }
                    catch { }

                    try
                    {
                        driver.FindElement(By.CssSelector("a[action='cancel']")).Click();
                        FunctionHelper.AddItemToListView(lstListView, Result.Uid, "Block");
                        return ReturnStatus.BLOCK;
                    }
                    catch { }

                    //get link
                    HttpRequest request = new HttpRequest();
                    request.AddHeader("Cookie", cookie);
                    request.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36");
                    string body = HttpUtility.HtmlDecode(request.Get($"https://mbasic.facebook.com/{Result.Uid}/allactivity?ref=bookmarks").ToString());

                    Match match = Regex.Match(body, $"href=\"\\/groups\\/{idGroup}\\?view=permalink&id=(.*?)&ref=bookmarks\"");
                    if (match.Success)
                    {
                        Result.CookieSuccess.Add($"https://www.facebook.com/{match.Groups[1].Value}/");
                        Console.WriteLine("----------------------> Success");
                        FunctionHelper.AddItemToListView(lstListView, Result.Uid, "Success");
                    }
                    else
                    {
                        Result.CookiePending.Add($"https://www.facebook.com/groups/{idGroup}/pending/");
                        Console.WriteLine("----------------------> Pending");
                        FunctionHelper.AddItemToListView(lstListView, Result.Uid, "Pending");
                    }
                }
                catch
                {
                    Console.WriteLine("===========================> Error");
                    //FunctionHelper.AddItemToListView(lstListView, Result.Uid, "Error");
                    return ReturnStatus.FAIL;
                }
            }

            return ReturnStatus.SUCCESS;
        }

        private void AddCookieToBrowser(string cookie, IWebDriver driver)
        {
            string[] ck = cookie.Split(';');
            foreach (string s in ck)
            {
                try
                {
                    string[] _c = s.Split("=".ToCharArray(), 2);
                    Cookie cc = new Cookie(_c[0].Trim(), _c[1].Trim());
                    driver.Manage().Cookies.AddCookie(cc);
                    if (_c[0].Trim() == "c_user")
                    {
                        Result.Uid = _c[1].Trim();
                    }
                }
                catch { }
            }
        }
    }
}
